import os
import json
import http
import time
from flask import Flask
from flask import request
from tornado.ioloop import IOLoop
from tornado.wsgi import WSGIContainer
from tornado.httpserver import HTTPServer

import env
import inference as infer

app = Flask(__name__)
app.config['JSON_AS_ASCII']=False


@app.route('/setup_inference',methods=['POST'])
def setup_inference():
    '''setup model for inference'''
    settings = infer.get_inference_settings(json_path=env.JSON_PATH)
    model = infer.setup_model(settings)
    processor= infer.setup_processor(settings)
    return json.dumps({'status':'OK'})

@app.route('/inference',methods=['POST'])
def inference():
    '''inference img'''
    settings = infer.get_inference_settings(json_path=env.JSON_PATH)
    results = []
    for f in request.files.getlist('file'):
        bytes_data = f.read()
        result = infer.model_inference(bytes_data, settings)
        results.append(result)
    return json.dumps({'result':results})

if __name__ == '__main__' :
    host = '0.0.0.0'
    port = env.PORT
    http_server = HTTPServer(WSGIContainer(app), max_buffer_size=10485760000)
    http_server.listen(port, address=host)
    print(f'Start Launch HTTP Server {host}:{port}')
    try:
        IOLoop.instance().start()
    except:
        os._exit(0)
        IOLoop.instance().stop()