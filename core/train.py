import os
import time
import torch
import logging
import numpy as np

from torch import nn
from torch import optim
from typing import Dict, List
from dataclasses import dataclass
from torch.utils.data import DataLoader
from torch.optim.lr_scheduler import StepLR

import env
import utils
import inference as infer
from datasets import TrainProcessor, TrainDataSet
from torch_model.model import Model



class nllloss(nn.Module):
    '''NLLLoss'''
    def __init__(self):
        super().__init__()
    def forward(self, x, y):
        eps=1e-32
        x = torch.clamp(x,eps,1.)
        loss = nn.NLLLoss()(torch.log(x),y.argmax(-1))
        return loss


def init_logger(log_path):
    '''Init Logger'''
    logger = logging.getLogger()
    logger.setLevel(logging.INFO)
    formatter = logging.Formatter('%(asctime)s - %(levelname)s - %(message)s')
    file_handler = logging.FileHandler(log_path)
    file_handler.setFormatter(formatter)
    logger.addHandler(file_handler)
    return logger


@dataclass
class TrainSettings(metaclass=utils.SingletonMetaclass):
    '''Train Settings'''
    train_folder: str
    val_folder: str
    resize_width: int
    resize_height: int
    class_map: dict
    pt_path: str
    batch_size: int
    epochs: int
    last_channel: int
    device: torch.device
    input_shape: List[int]
    project_name: str


def parsing_train_setting(json_path:str) -> dict:
    '''Parsing train Setting'''
    settings = utils.read_json(json_path)
    if len(settings) == 0:
        raise Exception('Inference Settings is empty.')
    train_folder = settings['train']['dataset']['train_folder']
    val_folder = settings['train']['dataset']['val_folder']
    resize_width = settings['train']['size']['width']
    resize_height = settings['train']['size']['height']
    class_map = settings['train']['class_map']
    pt_path = settings['train']['model']['pt_path']
    batch_size = settings['train']['model']['batch_size']
    epochs = settings['train']['model']['epochs']
    last_channel = len(class_map)
    device = utils.set_gpu(settings['device']['gpu_id'])
    input_shape = [1, 3, resize_height, resize_width]
    project_name = settings['project_name']
    return {'train_folder': train_folder, 'val_folder': val_folder, 'resize_width': resize_width, 'resize_height': resize_height, 'class_map': class_map, 'pt_path': pt_path, 'batch_size': batch_size, 'epochs': epochs, 'last_channel': last_channel, 'device': device, 'input_shape': input_shape, 'project_name': project_name}

def model_train(model, criterion, optimizer, scheduler, dataloaders, settings) -> nn.Module:
    '''Model Train
        Args:
            model: model to train
            criterion: loss function
            optimizer: optimizer
            scheduler: scheduler
            dataloaders: {'train': train_dataloader}
            settings: train settings
            num_epochs: number of epochs
    '''
    device = settings.device
    class_map = settings.class_map
    num_epochs = settings.epochs
    project_name = settings.project_name
    project_folder = f'{env.WEIGHT_FOLDER}/{project_name}'
    os.makedirs(project_folder, exist_ok=True)
    logger = init_logger(f'{project_folder}/train.log')
    start_time = time.time()
    for epoch in range(num_epochs):
        if epoch == 4:
            print('---- Gpu memory ----')
            os.system('gpustat --json')
            print('====='*15)
        for phase in ['train', 'val']:
            per_epoch_start_time = time.time()
            if phase == 'train':
                model.train()
            else:
                model.eval()
            bb_loss, bb_corrects = torch.tensor(0.).to(device), torch.tensor(0).to(device)
            for inputs, labels in dataloaders[phase]:
                inputs = inputs.to(device)
                labels = labels.to(device)
                with torch.set_grad_enabled(phase=='train'):
                    outputs = model(inputs)
                    _, preds = torch.max(outputs, -1)
                    label_one_hot = utils.one_hot(labels.data, len(class_map))
                    label_one_hot = torch.tensor(label_one_hot).to(device)
                    loss = criterion(outputs, label_one_hot)
                    if phase == 'train':
                        optimizer.zero_grad()
                        loss.backward()
                        optimizer.step()
                bb_loss += loss.item() * inputs.size(0)
                bb_corrects += torch.sum(preds == labels.data)
            if phase == 'val':
                scheduler.step()
            epoch_loss = bb_loss / len(dataloaders[phase].dataset)
            epoch_acc = bb_corrects / len(dataloaders[phase].dataset)
            result_log = f'Total Epoch:{num_epochs}, Epoch:{epoch+1}, {phase} Loss: {epoch_loss:.4f} Acc: {epoch_acc:.4f} Lr:{optimizer.param_groups[0]["lr"]} Epoch-Time: {(time.time() - per_epoch_start_time) // 60:.0f}min {(time.time() - per_epoch_start_time) % 60:.0f}sec'
            logger.info(result_log)
            print(result_log)
            
        print('-' * 30)
        torch.save(model.state_dict(), f'{project_folder}/latest.pth')
    time_elapsed = time.time() - start_time
    total_time_log = f'Total training time: {time_elapsed // 60:.0f}min {time_elapsed % 60:.0f}sec'
    print(total_time_log)
    logger.info(total_time_log)

    return model

def setup_model(settings:TrainSettings) -> nn.Module:
    '''Setup Model for Training'''
    pt_path = settings.pt_path
    input_shape = settings.input_shape
    last_channel = settings.last_channel
    device = settings.device
    return Model(pt_path, input_shape, last_channel, device, task_type='train', setup_f16=False).get_model()

def get_train_settings(json_path:str) -> TrainSettings:
    '''Get Train Settings'''
    settings = parsing_train_setting(json_path)
    print(f'settings:{settings}')
    return TrainSettings(**settings)

def get_train_loader(root_folder:str, settings:TrainSettings) -> DataLoader:
    '''Get Train Loader'''
    batch_size = settings.batch_size
    class_map = settings.class_map
    transform = TrainProcessor(settings)
    train_data = TrainDataSet(root_folder, transform=transform, class_map=class_map)
    train_loader = DataLoader(dataset=train_data, batch_size=batch_size, shuffle=True)
    return train_loader

def start_training(json_path:str) -> nn.Module:
    '''Start Training Process'''
    settings = get_train_settings(json_path)
    train_folder = settings.train_folder
    val_folder = settings.val_folder
    train_loader = get_train_loader(train_folder, settings)
    val_folder = get_train_loader(val_folder, settings)
    dataloaders = {'train': train_loader, 'val': val_folder}
    model = setup_model(settings)
    criterion = nllloss()
    optimizer = optim.Adam(model.parameters(), lr=1e-5)
    scheduler = StepLR(optimizer, step_size=25, gamma=0.6)
    model = model_train(model, criterion, optimizer, scheduler, dataloaders, settings)
    return model

def valid_prediction(model:nn.Module, json_path:str):
    '''Valid Prediction for Model by NG Image'''
    print('--- Valid Prediction ---')
    model.eval()
    settings = get_train_settings(json_path)
    val_folder = settings.val_folder
    class_map = settings.class_map
    inference_settings = infer.get_inference_settings(json_path)
    
    # get one ng image to valid
    val_img_name = os.listdir(os.path.join(val_folder, class_map["1"]))[0]
    data_path = os.path.join(val_folder, class_map["1"], val_img_name)
    print('data_path:', data_path)
    with open(data_path, 'rb') as f:
        img_data = f.read()
    data = infer.preprocessing_data(img_data, inference_settings)
    with torch.no_grad():
        prediction = model(data)
    print('prediction:', prediction)
    prediction = utils.to_numpy(prediction)
    result = utils.parse_classification(prediction, class_map)
    print('result:', result)




if __name__ == "__main__":
    args = utils.parse_config()
    utils.set_env(env, args)
    print('JSON_PATH:', env.JSON_PATH)
    model = start_training(env.JSON_PATH)
    # valid_prediction(model, env.JSON_PATH)
    

    
    