import os
import cv2
import json
import torch
import argparse
import numpy as np

class SingletonMetaclass(type):
    '''Singleton Metaclass'''
    def __init__(self, *args, **kwargs):
        self.__instance = None
        super().__init__(*args, **kwargs)
    def __call__(self, *args, **kwargs):
        if self.__instance is None:
            self.__instance = super().__call__(*args, **kwargs)
            return self.__instance
        else:
            return self.__instance
        
        
def read_img_to_bytes(img_path):
    '''Read Image to bytes'''
    with open(img_path, 'rb') as file:
        bytes_data = file.read()
    return bytes_data

def read_path(img_path):
    '''Read Image Path to numpy'''
    return cv2.imread(img_path, cv2.IMREAD_UNCHANGED)

def read_bytes(img_bytes):
    '''Read Image Bytes to numpy'''
    return cv2.imdecode(np.fromstring(img_bytes,np.uint8),1)

def read_json(json_path):
    '''Read Json File'''
    with open(json_path, 'r') as f:
        json_data = json.loads(f.read())
    return json_data

def write_json(json_path, json_data):
    '''Write Json File'''
    with open(json_path, 'w') as f:
        f.write(json.dumps(json_data, indent=4))

def resize(img, width, height):
    '''Resize Image to (width, height)'''
    if width > 0 and height > 0:
        return cv2.resize(img, (width,height),cv2.INTER_CUBIC)
    else:
        raise f'width or height error: {width} {height}'

def convert_color(image):
    '''Convert Image Color to RGB'''
    if len(image.shape)==2:
        channel=1
    else :
        channel = image.shape[-1]
    if channel==4:
        original = "BGRA"
    else:
        original = 'BGR'
    mode = f'cv2.COLOR_{original}2RGB'
    return cv2.cvtColor(image,eval(mode))

def expand_dim(img):
    '''Expand Image Dimension to (1, height, width, channel)'''
    return np.expand_dims(img, axis=0)

def transpose(img):
    '''Transpose Image to (channel, height, width) or (batch, channel, height, width)'''
    if img.ndim == 4:
        argsort = [0,3,1,2]
    elif img.ndim == 3:
        argsort = [2,1,0]
    else:
        raise ValueError('img.ndim must be 3 or 4')
    return np.transpose(img, argsort)

def img_norm(img):
    '''Normalize Image to (0, 1)'''
    return img/255.

def to_numpy(tensor):
    '''Convert Tensor to numpy array'''
    if isinstance(tensor, np.ndarray):
        return tensor
    elif isinstance(tensor, torch.Tensor):
        return tensor.detach().cpu().numpy() if tensor.requires_grad else tensor.cpu().numpy()
    elif isinstance(tensor, list):
        return np.array(tensor)
    else:
        raise TypeError(f'Cannot convert {type(tensor)} to numpy array.')

def parse_classification(prediction, class_map):
    '''Parse Classification Result'''
    results = {}
    nums = len(prediction)
    class_index = np.argmax(prediction,axis=-1)
    class_scores = np.max(prediction,axis=-1)
    class_names = [class_map[(str(x))] for x in class_index]
    for i in range(nums):
        results[i] = {'prediction':class_names[i], 'score':float(class_scores[i])}
    return results

def check_img(f_path):
    '''Check Whether the File is Image'''
    ext = os.path.splitext(f_path)[-1]
    img_ext = ['.jpg', '.jpeg', '.png', '.bmp', '.tiff', '.tif']
    return ext.lower() in img_ext

def one_hot(labels, num_class):
    '''Convert Label to One Hot'''
    one_hot_list = []
    for label in labels:
        single_list = [0 for _ in range(num_class)]
        single_list[label] = 1
        one_hot_list.append(single_list)
    return one_hot_list

def parse_config():
    '''Parse Config'''
    main_parser = argparse.ArgumentParser()
    main_parser.add_argument(
        '-P','--port',help='set port',default='8001')
    main_parser.add_argument(
        '-J','--json_path',help='set json path',default='./setting.json')
    main_args = main_parser.parse_args()
    args = {
        'port': int(main_args.port),
        'json_path':str(main_args.json_path)
    }
    return args

def set_env(env, args):
    '''Set Environment'''
    env.PORT = args['port']
    env.JSON_PATH = args['json_path']

def set_gpu(gpu_id):
    '''Set GPU'''
    if torch.cuda.is_available():
        return torch.device(f'cuda:{gpu_id}')
    else:
        return torch.device('cpu')