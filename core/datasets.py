
from typing import ClassVar
from dataclasses import dataclass

import os
import torch
import numpy as np 

from torch.utils.data import Dataset
import utils



@dataclass
class InferenceProcessSettings:
    '''InferenceProcessSettings is a class for setting up preprocessing settings for inference.'''
    resize_width: int
    resize_height: int
    setup_f16: bool
    device: torch.device

@dataclass
class TrainProcessSettings:
    '''TrainProcessSettings is a class for setting up preprocessing settings for training.'''
    resize_width: int
    resize_height: int


class InferenceProcessor(metaclass=utils.SingletonMetaclass):
    '''InferenceProcessor is a class for preprocessing image data for inference.'''
    def __init__(self, process_settings:InferenceProcessSettings):
        self.process_settings = process_settings
    
    def process(self, data:bytes) -> torch.Tensor:
        '''process is a function for preprocessing image data for inference.'''
        return self._process_pipeline(data)

    def _process_pipeline(self, data:bytes) -> torch.Tensor:
        img_np = utils.read_bytes(data)
        resize_width, resize_height = self.process_settings.resize_width, self.process_settings.resize_height
        img_resize = utils.resize(img_np, resize_width, resize_height)
        img_rgb = utils.convert_color(img_resize)
        img_batch = utils.expand_dim(img_rgb)
        img_trans = utils.transpose(img_batch)
        img_norm = utils.img_norm(img_trans)
        # print('np.max:', np.max(img_norm), 'np.mean:', np.mean(img_norm))
        img_torch = torch.from_numpy(img_norm)
        img = img_torch.to(self.process_settings.device)
        if self.process_settings.setup_f16:
            img = img.half()
        return img


class TrainProcessor:
    '''TrainProcessor is a class for preprocessing image data for training.'''
    def __init__(self, process_settings:TrainProcessSettings):
        self.process_settings = process_settings
    
    def process(self, data:bytes) -> torch.tensor:
        '''process is a function for preprocessing image data for training.'''
        return self._process_pipeline(data)

    def _process_pipeline(self, data:bytes) -> torch.tensor:
        img_np = utils.read_bytes(data)
        resize_width, resize_height = self.process_settings.resize_width, self.process_settings.resize_height
        img_resize = utils.resize(img_np, resize_width, resize_height)
        img_rgb = utils.convert_color(img_resize)
        # img_batch = utils.expand_dim(img_rgb) #torch loader already expand dim
        img_batch = img_rgb
        img_trans = utils.transpose(img_batch)
        img_norm = utils.img_norm(img_trans)
        img_torch = torch.from_numpy(img_norm)
        img = img_torch.to(self.process_settings.device).float()
        return img
    

class TrainDataSet(Dataset):
    '''TrainDataSet is a class for setting up data for training.
        Description:
            root_folder: the folder path including class folder'''
    def __init__(self, root_folder, transform, class_map):
        self.root_folder = root_folder
        self.transform = transform
        self.class_map = class_map
        self.inverse_class_map = {v:int(k) for k, v in class_map.items()}
        self.cls_list = os.listdir(self.root_folder)
        self.data_dict = self._get_data_dict(self.root_folder, self.cls_list)
        
        
    def __getitem__(self, idx):
        img_path = self.data_dict[idx][0]
        with open(img_path, 'rb') as f:
            img_bytes = f.read()
        img = self.transform.process(img_bytes)
        label = self.data_dict[idx][1]
        label = torch.tensor(label)
        return img, label
    
    def __len__(self):
        return len(self.data_dict)
    
    def _get_data_dict(self, folder_path, cls_list):
        data_dict = {}
        idx = 0
        for cls in cls_list:
            cls_folder = os.path.join(folder_path, cls)
            for f in os.scandir(cls_folder):
                if utils.check_img(f.path):
                    data_dict[idx] = (f.path, self.inverse_class_map[cls])
                    idx += 1
        return data_dict
