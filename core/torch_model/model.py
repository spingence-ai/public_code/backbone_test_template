import torch
from torch import nn

import utils
from . import  model_core


class Model(metaclass=utils.SingletonMetaclass):
    '''Model class for inference and training'''
    def __init__(self, pt_path, input_shape, last_channel, device, task_type, setup_f16):
        self.pt_path = pt_path
        self.input_shape = input_shape
        self.last_channel = last_channel
        self.device = device
        self.task_type = task_type
        self.setup_f16 = setup_f16
        self.model = None
        if task_type == 'inference':
            self.setup_model_inference()
        elif task_type == 'train':
            self.setup_model_train()
        else:
            raise Exception('task_type must be inference or train')
    
    def setup_model_inference(self):
        '''Setup model for inference
            Args:
                pt_path: path to model, if True, load weight from path
                setup_f16: True or False, if True, setup model to use f16
                set_cuda: setup model to use cuda
                set_eval: setup model to use eval
        '''
        self.model = self.build_model(self.input_shape, self.last_channel)
        if self.pt_path:
            self.load_weight(self.pt_path)
        if self.setup_f16:
            self.set_f16()
        self.set_cuda()
        self.model.eval()
    
    def setup_model_train(self):
        '''Setup model for training'''
        self.model = self.build_model(self.input_shape, self.last_channel)
        if self.pt_path:
            self.load_weight(self.pt_path)
        else:
            raise Exception('No pretrained weight be provided for training')
        self.set_cuda()
        self.model.train()
    
    def build_model(self, input_shape:list, last_channel:int) -> nn.Module:
        '''Build model from scratch'''
        return model_core.build_model(input_shape, last_channel, pretrain=True)
        
    def load_weight(self, pt_path:str):
        '''Load weight from path'''
        print(f'--- Load weight: {pt_path} ---')
        state_dict = torch.load(pt_path)
        state_dict = intersect_dicts(state_dict, self.model.state_dict(), exclude=()) 
        self.model.load_state_dict(state_dict, strict=False)
    
    def set_cuda(self):
        '''Set model to use cuda'''
        self.model.to(self.device)
    
    def set_f16(self):
        '''Set model to use f16'''
        print('---Set model to use f16---')
        self.model = self.model.half()
    
    def get_model(self):
        '''Return model'''
        return self.model


def intersect_dicts(pretrain_state_dict, model_state_dict, exclude=()):
    return {k: v for k, v in pretrain_state_dict.items() if k in model_state_dict and not any(x in k for x in exclude) and v.shape == model_state_dict[k].shape}
