

import torchvision
from torch import nn
from torchvision.models import mobilenet_v3_small, resnet18


def build_model(input_shape, last_channel, pretrain=True):
    '''Build model from scratch
        Args:
            input_shape: [BS, C, H, W]
            last_channel: number of classes
            pretrain: True or False, if True, load pretrained model
    '''
    model = mobilenet_v3_small(pretrained=pretrain, progress=True)
    # view the name and module to change input_shape/last_channel
    # for name, module in model.named_modules():
    #     print(name, module)

    ### edit the model structure ###
    model.classifier = nn.Sequential(
        nn.Linear(576, last_channel),
        nn.Softmax(dim=-1)
    )
    return model


#restnet18 example
# def build_model(input_shape, last_channel, pretrain=True):
#     '''Build model from scratch
#         Args:
#             input_shape: [BS, C, H, W]
#             last_channel: number of classes
#             pretrain: True or False, if True, load pretrained model
#     '''
#     model = resnet18(pretrained=pretrain, progress=True)
#     # view the name and module to change input_shape/last_channel
#     # for name, module in model.named_modules():
#     #     print(name, module)

#     ### edit the model structure ###
#     model.fc = nn.Sequential(
#         nn.Linear(512, last_channel),
#         nn.Softmax(dim=-1))
    
#     return model