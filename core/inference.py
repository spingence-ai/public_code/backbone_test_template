import os
import time
import torch
import logging
import numpy as np
from torch import nn
from thop import profile
from typing import Dict, List
from dataclasses import dataclass, field

import env
import utils
from datasets import InferenceProcessor
import torch_model.model_core as model_core
from torch_model.model import Model

@dataclass
class InferenceSettings(metaclass=utils.SingletonMetaclass):
    '''Inference Settings'''
    resize_width: int
    resize_height: int
    class_map: dict
    pt_path: str
    setup_f16: bool
    last_channel: int
    device: torch.device
    input_shape: List[int]
    test_folder: str
    project_name: str


def parsing_inference_setting(json_path:str) -> dict:
    '''Parsing Inference Setting'''
    settings = utils.read_json(json_path)
    if len(settings) == 0:
        raise Exception('Inference Settings is empty.')
    resize_width = settings['inference']['size']['width']
    resize_height = settings['inference']['size']['height']
    class_map = settings['inference']['class_map']
    pt_path = settings['inference']['model']['pt_path']
    setup_f16 = settings['inference']['model']['setup_f16']
    last_channel = len(class_map)
    device = utils.set_gpu(settings['device']['gpu_id'])
    input_shape = [1, 3, resize_height, resize_width]
    test_folder = settings['inference']['dataset']['test_folder']
    project_name = settings['project_name']
    inference_settings = {'resize_width': resize_width, 'resize_height': resize_height, 'class_map': class_map, 'pt_path': pt_path, 'setup_f16': setup_f16, 'last_channel': last_channel, 'device': device, 'input_shape': input_shape, 'test_folder': test_folder, 'project_name': project_name}
    return InferenceSettings(**inference_settings)

def setup_model(settings:InferenceSettings) -> nn.Module:
    '''Setup Model for Inference'''
    pt_path = settings.pt_path
    input_shape = settings.input_shape
    last_channel = settings.last_channel
    device = settings.device
    task_type = 'inference'
    setup_f16 = settings.setup_f16
    return Model(pt_path, input_shape, last_channel, device, task_type, setup_f16).get_model()

def preprocessing_data(bytes_data:bytes, processor:InferenceProcessor) -> torch.Tensor:
    '''Preprocessing Data'''
    return processor.process(bytes_data)

def postprocessing_data(prediction:torch.Tensor, settings:InferenceSettings) -> dict:    
    '''Postprocessing Data'''
    class_map = settings.class_map
    prediction = utils.to_numpy(prediction)
    result = utils.parse_classification(prediction, class_map)
    return result

def get_inference_settings(json_path:str) -> InferenceSettings:
    '''Get Inference Settings'''
    return parsing_inference_setting(json_path)

def setup_processor(settings:InferenceSettings):
    '''Get Inference Processor'''
    global processor
    processor = InferenceProcessor(settings)
    return processor

@torch.no_grad()
def model_inference(bytes_data:bytes, settings:InferenceSettings) -> dict:
    '''Model Inference'''
    model = setup_model(settings)
    data = preprocessing_data(bytes_data, processor)
    prediction = model(data)
    result = postprocessing_data(prediction, settings)
    return result

@torch.no_grad()
def check_process_time(bytes_data:bytes, settings:InferenceSettings, logger:logging) -> dict:
    '''Check Process Time'''
    setup_model_time = time.time()
    model = setup_model(settings)
    logger.info(f'setup-model-time: {(time.time()-setup_model_time)*1000} ms')
    data_time = time.time()
    data = preprocessing_data(bytes_data, processor)
    logger.info(f'preprocessing-time: {(time.time()-data_time)*1000} ms')
    predict_time = time.time()
    prediction = model(data)
    logger.info(f'predict-time: {(time.time()-predict_time)*1000} ms')
    post_time = time.time()
    result = postprocessing_data(prediction, settings)
    logger.info(f'postprocessing-time: {(time.time()-post_time)*1000} ms')
    logger.info(f'{"-----"*15}')
    return result

def init_logger(log_path:str) -> logging:
    '''Init Logger'''
    logger = logging.getLogger()
    logger.setLevel(logging.INFO)
    formatter = logging.Formatter('%(message)s')
    file_handler = logging.FileHandler(log_path, mode='w')
    file_handler.setFormatter(formatter)
    logger.addHandler(file_handler)
    return logger


if __name__ == "__main__":
    settings = get_inference_settings(json_path=env.JSON_PATH)
    width = settings.resize_width
    img_path = f"{env.TEST_FOLDER}/test_{width}.jpg"
    print(f'---- test size:{width} ----')
    with open(img_path, 'rb') as f:
        bytes_data = f.read()
    # logger = init_logger('./inference.log')
    # for i in range(5):
    #     result = check_process_time(bytes_data, settings, logger)
    

    model = setup_model(settings)
    processor = setup_processor(settings)
    data = preprocessing_data(bytes_data, processor)
    for i in range(100):
        if i == 5:
            input = torch.randn(1, 3, 640, 640).to('cuda').half()
            flops, params = profile(model, inputs=(input, ))
            GFLOPs = flops/1e9
            print(f'---- GFLOPs:{round(GFLOPs, 2)} GB----')
            print('---- Gpu memory ----')
            os.system('gpustat')
            print('====='*15)
        prediction = model(data)
    
    time_list = []
    for i in range(1000):
        start_time = time.time()
        prediction = model(data)
        torch.cuda.synchronize()
        end_time = time.time() - start_time
        time_list.append(end_time)
        
    print('---- 純 Inference time ----')
    print('Mean-time:', round(np.mean(time_list)*1000, 1), 'ms')
    print('Std-time:', round(np.std(time_list)*1000, 1), 'ms')
    print('====='*15)
    