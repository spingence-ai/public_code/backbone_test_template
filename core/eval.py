import os
import shutil
import numpy as np
from sklearn.metrics import confusion_matrix

import env
import utils
import inference as infer


def file_result_parsing(img_name:str, result:dict, ground_truth:str) -> dict:
    '''File Result Parsing
        Input:
            result:dict
                {
                    'prediction':str, 'score':float
                }
        
        Output: 
            file_result:dict
                {   
                    "img_name":{
                        "prediction":str, "score":float, "ground_truth":str
                    }
                }
        '''
    file_result = {}
    prediction = result['prediction']
    score = result['score']
    file_result[img_name] = {"prediction":prediction, "score":score, "ground_truth":ground_truth}
    return file_result

def get_folder_result(cls_list:list, settings:infer.InferenceSettings) -> dict:
    '''Get Folder Result
        Input:
            cls_list:list
                ['NG', 'OK']

        Output:
            folder_result:dict
                {
                    "NG": {   
                            "img_name":{
                                "prediction":str, "score":float, "ground_truth":str
                            }
                        },
                    "OK": {...},
                }
    '''
    test_folder_path = settings.test_folder
    folder_result = {cls:{} for cls in cls_list}
    for cls in cls_list:
        for f in os.scandir(f'{test_folder_path}/{cls}'):
            if utils.check_img(f.name):
                bytes_data = utils.read_img_to_bytes(f.path)
                # get batch index 0 result
                result = infer.model_inference(bytes_data, settings)[0]
                file_result = file_result_parsing(f.name, result, cls)
                folder_result[cls].update(file_result)
    return folder_result
    
def parsing_all_result(folder_result:dict) -> dict:
    '''Parsing All Result
    Input:
        folder_result:dict
            {
                "NG": {   
                        "img_name":{
                            "prediction":str, "score":float, "ground_truth":str
                        }
                    },
                "OK": {...},
            }
    Output:
        all_result:dict
            {
                "prediction":[str,str,...], 
                "ground_truth":[str,str,...]
            }
    '''
    all_result = {}
    all_result['prediction'] = []
    all_result['ground_truth'] = []
    for cls, cls_map in folder_result.items():
        for file_name, file_map in cls_map.items():
            all_result['prediction'].append(file_map['prediction'])
            all_result['ground_truth'].append(file_map['ground_truth'])
    return all_result

def parse_cm_single(cm:np.array):
    """
    Parse confusion matrix to TP,TN,FN,FP
    Input:
        cm:np.array
        [[TP,FN],
        [FP,TN]]
    """
    TP,TN,FN,FP = cm[-2,-2],cm[-1,-1],cm[-2,-1],cm[-1,-2]
    return TP,TN,FN,FP

e = 0.00000000001
def cal_accuracy(TP,TN,FN,FP):
    """Accuracy"""
    return (TP+TN)/(TP+TN+FP+FN+e)

def cal_FPR(TP,TN,FN,FP):
    """FPR: False Positive Rate / Overkill Rate"""
    return FP/(FP+TN+e)

def cal_FNR(TP,TN,FN,FP):
    """FNR: False Negtive Rate / Underkill Rate"""
    return FN/(FN+TP+e)

def cal_metrics(cls_result:dict, classes=['NG','OK']) -> dict:
    """
    Calculate metrics for classification
    Input:
        cls_result:dict
            ground_truth:list
                ['NG'...]
            prediction:list
                ['OK'...]
        classes:list
            ['NG', 'OK']
    Intermediate value:
        cm:np
            [[TP,FN],
            [FP,TN]]
    Output:
        metrics:dict
            {'accuracy':float...}
    """
    ground_truth = cls_result['ground_truth']
    prediction = cls_result['prediction']
    cm = confusion_matrix(ground_truth,prediction,labels=classes)
    TP,TN,FN,FP = [int(x) for x in parse_cm_single(cm)]
    cate_list = {
        'TP':TP,
        'TN':TN,
        'FN':FN,
        'FP':FP
    }
    metric_list = ['accuracy','FPR','FNR']
    metrics = {}
    metrics['Statistics'] = cate_list
    if classes == ['NG', 'OK']:
        metrics['Total_Num'] = {'NG':FN+TP, 'OK':TN+FP}

    for metric_name in metric_list:
        metrics[metric_name]=round(100*eval(f"cal_{metric_name}(TP,TN,FN,FP)"),2)
    
    return metrics

def gen_whole_result(folder_result:dict, metrics:dict, settings:infer.InferenceSettings):
    '''Generate Whole Result and Save to JSON
    Intermediate value:
        whole_result:dict
            {
                "NG": {   
                        "img_name":{
                            "prediction":str, "score":float, "ground_truth":str
                        }
                    },
                "OK": {...},
                "metrics":{
                    'accuracy':float, 'FPR':float, 'FNR':float
                }
            }
    '''
    project_name = settings.project_name
    whole_result = {}
    whole_result.update(folder_result)
    whole_result['metrics'] = metrics
    project_folder = f'{env.RESULT_FOLDER}/{project_name}'
    os.makedirs(project_folder, exist_ok=True)
    utils.write_json(f'{project_folder}/pred_result.json', whole_result)

def save_underkill_overkill_img(folder_result:dict, settings:infer.InferenceSettings):
    '''Save Underkill and Overkill Image'''
    test_folder_path = settings.test_folder
    project_name = settings.project_name
    underkill_folder = f'{env.RESULT_FOLDER}/{project_name}/underkill'
    overkill_folder = f'{env.RESULT_FOLDER}/{project_name}/overkill'
    os.makedirs(underkill_folder, exist_ok=True)
    os.makedirs(overkill_folder, exist_ok=True)
    for cls, cls_map in folder_result.items():
        for file_name, file_map in cls_map.items():
            if file_map['prediction'] != file_map['ground_truth']:
                src_path = f'{test_folder_path}/{cls}/{file_name}'
                if file_map['ground_truth'] == 'NG':
                    shutil.copy2(src_path, f'{underkill_folder}/{file_name}')
                elif file_map['ground_truth'] == 'OK':
                    shutil.copy2(src_path, f'{overkill_folder}/{file_name}')
                else:
                    raise ValueError(f'Ground Truth Error:{file_map["ground_truth"]}')

def start_eval(json_path:str, save_img=False) -> dict:
    '''Start Evaluation'''
    settings = infer.get_inference_settings(json_path=env.JSON_PATH)
    model = infer.setup_model(settings)
    processor = infer.setup_processor(settings)
    test_folder_path = settings.test_folder
    class_map = settings.class_map
    project_name = settings.project_name
    cls_list = list(class_map.values())
    folder_result = get_folder_result(cls_list, settings)
    if save_img:
        save_underkill_overkill_img(folder_result, settings)
    all_result = parsing_all_result(folder_result)
    metrics = cal_metrics(all_result)
    gen_whole_result(folder_result, metrics, settings)
    return metrics


if __name__ == '__main__':
    args = utils.parse_config()
    utils.set_env(env, args)
    print('JSON_PATH:', env.JSON_PATH)
    metrics = start_eval(env.JSON_PATH)

    print(f'Top1_Acc: {round(metrics["accuracy"], 1)} %')
    print(f'FNR: {round(metrics["FNR"], 1)} %')
    print(f'FPR: {round(metrics["FPR"], 1)} %')
    print('+++'*20)