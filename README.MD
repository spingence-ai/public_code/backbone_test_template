# backbone_test_template
# WorkFlow
```mermaid
flowchart LR
A(環境安裝)-->B(模型更換)-->C(inference測試)-->D(training測試)
```
# A: 環境安裝
1. 安裝python 3.9.16 和 torch 1.10.1+cu113
    ```python
    conda create -n backbone python=3.9.16
    conda activate backbone
    pip install torch==1.10.1+cu113 torchvision==0.11.2+cu113 torchaudio==0.10.1 -f https://download.pytorch.org/whl/torch_stable.html
    ```

2. 安裝其他套件
    ```python
    pip install -r requirement.txt
    ```

# B: 模型更換
1. 更改./core/torch_model/model_core.py裡的build_model function, 使其建立指定模型, 並針對模型的input_shape, last_channel可做更動
    ```python
    def build_model(input_shape, last_channel, pretrain=True):
        '''Build model from scratch
            Args:
                input_shape: [BS, C, H, W]
                last_channel: number of classes
                pretrain: True or False, if True, load pretrained model
        '''
        model = mobilenet_v3_small(pretrained=pretrain, progress=True)
        model.classifier = nn.Sequential(
            nn.Linear(576, last_channel),
            nn.Softmax(dim=-1)
        )
        return model
    ```
# C: inference測試
1. 更改./core/setting.json, 之後模型會依setting去建立
    1. 注意：
        1. build_model function建立模型後，若有pt_path會load weight進來
        2. 若width, height有做更動, f"./core/test_imgs/test_{width}.jpg" 也會做同等縮放更動
    ```json
    {
        "inference": {
            "model": {
                "pt_path": "",
                "setup_f16": true
            },
            "size": {
                "width": 640,
                "height": 640
            },
            "class_map": {
                "0": "OK",
                "1": "NG"
            }
        },
        "device": {
            "gpu_id": 0
        }
    }
    ```
2. 進行inference測試
    1. 注意:
        1. 以下先進行尺寸640測試，若時間和結果沒問題，再進行其他尺寸[128, 320, 800, 1280]測試
        2. 是否電源高效能、GPU超頻1800左右並鎖頻

    2. 進行純inference測試
        ```python
        python inference.py
        ```

    3. 進行整段inference測試
        1. 先在一個cmd 開 server
            ```python
            python server.py
            ```
        2. 再另一個cmd 執行 client
            ```python
            python client.py
            ```

# D: training 測試
1. 更改./core/setting.json, 之後模型會依setting去建立
    1. 注意：
        1. build_model function建立模型，需指定pt_path(pretrained weight), 會load pretrained weight進來
    ```json
    {
        "project_name": "xxxx",
        "train": {
            "dataset": {
                "train_folder": "",
                "val_folder": ""
            },
            "model": {
                "pt_path": "./weights/xxx",
                "batch_size": 16,
                "epochs": 10
            },
            "size": {
                "width": 640,
                "height": 640
            },
            "class_map": {
                "0": "OK",
                "1": "NG"
            }
        }
    }
    ```

2. 進行training測試, 會在./core/weights/{project_name}存下模型與訓練log
    ```python
    python train.py
    ```

3. eval測試，評估模型Accuracy, FNR(漏檢), FPR(過殺)
    1. 更改 ./core/setting.json 的 test_folder 和 pt_path 為驗証的資料集與模型
    ```json
        "inference": {
            "dataset": {
                "test_folder": "C://Users//User//Documents//Gitlab//test_dataset//mvtec_single//test"
            },
            "model": {
                "pt_path": "./weights/poject_name/latest.pth",
                "setup_f16": true
            },
            "size": {
                "width": 640,
                "height": 640
            },
            "class_map": {
                "0": "OK",
                "1": "NG"
            }
    }
    ```

    2. 進行eval測試
    ```python
    python eval.py
    ```

# E: 其他
1. 使用bash方式進行批次訓練和驗証
    1. 建立task.sh, 透過 -J 指定setting路徑
    ```python
        python train.py -J ./task_settings/bottle.json
        python train.py -J ./task_settings/carpet.json
    ```

    2. 執行task.sh
    ```python
    sh task.sh
    ```