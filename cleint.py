import os
import time
import requests
import numpy as np

import core.env as env
import core.utils as utils

def get_width() -> int:
    '''get width'''
    json_path = os.path.join('core', env.JSON_PATH)
    settings = utils.read_json(json_path=json_path)
    return settings['inference']['size']['width']

def load_img(width:int) -> bytes:
    '''load image'''
    img_path = os.path.join('core', env.TEST_FOLDER, f'test_{width}.jpg')
    with open(img_path,'rb') as f:
        img = f.read()
    return img

def setup_infer_model(base_url:str):
    '''setup model for inference'''
    setup_model_url = f"{base_url}/setup_inference"
    response = requests.post(setup_model_url, headers={'Connection':'close'})
    if response.status_code != 200:
        raise Exception('Setup Error')

def gettext(base_url:str, img:bytes) -> str:
    '''get response text from server'''
    inference_url = f"{base_url}/inference"
    response = requests.post(inference_url,files={'file':img},headers={'Connection':'close'})
    if response.status_code != 200:
        raise Exception(f'Inference Error, status code:{response.status_code}')
    if 'result' not in response.json():
        raise Exception(f'Format abnormal, {response.json()}')
    return response.json()

def speedtest(base_url:str, img:bytes) -> float:
    '''get inference time'''
    inference_url = f"{base_url}/inference"
    start_time=time.time()
    response = requests.post(inference_url,files={'file':img},headers={'Connection':'close'})
    end_time = time.time()
    return end_time-start_time


if __name__ == "__main__":
    base_url = f'http://127.0.0.1:{env.PORT}'
    setup_infer_model(base_url)
    width = get_width()
    img = load_img(width)
    
    print('---- check result and time ----')
    print(gettext(base_url,img))
    print(f'{round(speedtest(base_url,img)*1000, 1)} ms')
    print('====='*15)
    
    print(f'---- test size:{width} speed ----')
    for i in range(100):
        #warmup 100 times
        speedtest(base_url,img)
        
    time_list = []
    for j in range(1000):
        #calculate 1000 times
        time_list.append(speedtest(base_url,img))

    # mean and std inference time(ms)
    print('---- 整段 Inference time ----')
    print('mean-time:', round(np.mean(time_list)*1000, 1), 'ms')
    print('std-time:',round(np.std(time_list)*1000, 1), 'ms')
    print('====='*15)